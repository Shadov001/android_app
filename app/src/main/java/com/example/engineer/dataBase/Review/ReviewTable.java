package com.example.engineer.dataBase.Review;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.example.engineer.dataBase.Game.GameTable;

public class ReviewTable {

    public static final String REVIEWS = "Reviews";

    public static class ReviewColumns {
        public static final String ID = "Id";
        public static final String GAME = "GAME";
        public static final String AUTHOR = "AUTHOR";
        public static final String AUTHOR_NAME = "AUTHOR_NAME";
        public static final String CONTENT = "reviewContent";
        public static final String LIKES_COUNT = "likesCount";
    }

    public static void onCreate(SQLiteDatabase db) {
        String sql = "create table " + REVIEWS + " ( " +
                ReviewTable.ReviewColumns.ID + "  INTEGER PRIMARY KEY AUTOINCREMENT, " +
                ReviewColumns.GAME + " TEXT, " +
                ReviewColumns.AUTHOR + " TEXT, " +
                ReviewColumns.AUTHOR_NAME + " TEXT, " +
                ReviewColumns.CONTENT + " TEXT, " +
                ReviewTable.ReviewColumns.LIKES_COUNT + " INTEGER);";

        db.execSQL(sql);
    }


}
