package com.example.engineer.dataBase.Game;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.example.engineer.dataBase.Favorite.Favorite;
import com.example.engineer.dataBase.Favorite.FavoriteTable;
import com.example.engineer.dataBase.common.DAO;

import java.util.ArrayList;
import java.util.List;

public class GameDAO implements DAO<Game> {
    public SQLiteDatabase db;

    public GameDAO(SQLiteDatabase db) {
        this.db = db;
    }

    @Override
    public void save(Game game) {
        SQLiteStatement stmt = db.compileStatement( "insert into " + GameTable.GAMES + " ( " +
                //                GameTable.GameColumns.ID + ", " +
                GameTable.GameColumns.GAME_TITLE + ", " +
                GameTable.GameColumns.GAME_GENRE + ", " +
                GameTable.GameColumns.GAME_YEAR + ", " +
                GameTable.GameColumns.GAME_DESCRIPTION + ", " +
                GameTable.GameColumns.GAME_PC + ", " +
                GameTable.GameColumns.GAME_PS3  + ", " +
                GameTable.GameColumns.GAME_PS4 + ", " +
                GameTable.GameColumns.GAME_XONE  + ", " +
                GameTable.GameColumns.GAME_X360 + ", " +
                GameTable.GameColumns.GAME_NINTENDO +
                " ) values ( \'" +
//                game.getId() + "\', \'" +
                game.getTitle() + "\', \'" +
                game.getType() + "\', \'" +
                game.getYear() + "\', \'" +
                game.getDescription() + "\', \'" +
                game.getPCAvailable() + "\', \'" +
                game.getPS3Available() + "\', \'" +
                game.getPS4Available() + "\', \'" +
                game.getXBOXONEAvailable() + "\', \'" +
                game.getXBOXAvailable() + "\', \'" +
                game.getNintendoAvailable() +
                "\' )");
        stmt.execute();
        stmt.close();
    }

    public List<Game> getAll() {
        List<Game> games = new ArrayList<>();

        String sql = "select * from " + GameTable.GAMES + ";";
        Cursor cursor = db.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            Game game = new Game();
            game.setId(cursor.getInt(0));
            game.setTitle(cursor.getString(1));
            game.setType(cursor.getString(2));
            game.setYear(cursor.getInt(3));
            game.setDescription(cursor.getString(4));
            game.setPCAvailable(cursor.getInt(5));
            game.setPS3Available(cursor.getInt(6));
            game.setPS4Available(cursor.getInt(7));
            game.setXBOXONEAvailable(cursor.getInt(8));
            game.setXBOXAvailable(cursor.getInt(9));
            game.setNintendoAvailable(cursor.getInt(10));
            game.setLikesCount(cursor.getInt(11));

            games.add(game);
        }

        return games;
    }

    public void addLike(Integer gameId) {
        SQLiteStatement stmt = db.compileStatement( "update " +
                GameTable.GAMES + " SET " +
                GameTable.GameColumns.LIKES_COUNT + " = \'" +
                GameTable.GameColumns.LIKES_COUNT + " + 1 " +
                "\' WHERE " +
                GameTable.GameColumns.ID + " = " +
                gameId +";"
        );
        stmt.execute();
        stmt.close();
    }

//    public void addDislike(Integer gameId) {
//        SQLiteStatement stmt = db.compileStatement( "update " +
//                GameTable.GAMES + " SET " +
//                GameTable.GameColumns.DISLIKES_COUNT + " = \'" +
//                GameTable.GameColumns.DISLIKES_COUNT + " + 1 " +
//                "\' WHERE " +
//                GameTable.GameColumns.ID + " = " +
//                gameId +";"
//        );
//        stmt.execute();
//        stmt.close();
//    }
}
