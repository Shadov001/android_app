package com.example.engineer.dataBase.Likes;

import java.io.Serializable;

public class Like implements Serializable {
    private Integer Id;
    private String user;
    private String game;

    public Like(Integer id, String user, String game) {
        Id = id;
        this.user = user;
        this.game = game;
    }

    public Like() {
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }
}
