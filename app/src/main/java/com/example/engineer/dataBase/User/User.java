package com.example.engineer.dataBase.User;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.media.Image;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import java.io.Serializable;

public class User implements Serializable {
    private Integer Id;
    private String userEmail;
    private String userPassword;
    private Integer userType;
    private String userName;
    private String userSurname;
    private Double userLoyaltyPoints;
    private String userNickname;

    public User(String email, String password, String nickname, Integer type, String name, String surname, Double loyaltyPoints) {
        userEmail = email;
        userNickname = nickname;
        userPassword = password;
        userType = type;
        userName = name;
        userSurname = surname;
        userLoyaltyPoints = loyaltyPoints;
    }

    public User(){

    }

    public String getName() {
        return userName;
    }

    public String getSurname() {
        return userSurname;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer type) {
        userType = type;
    }

    public void setName(String name) {
        userName = name;
    }

    public void setSurname(String surname) {
        userSurname = surname;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserSurname() {
        return userSurname;
    }

    public void setUserSurname(String userSurname) {
        this.userSurname = userSurname;
    }

    public Double getUserLoyaltyPoints() {
        return userLoyaltyPoints;
    }

    public void setUserLoyaltyPoints(Double userLoyaltyPoints) {
        this.userLoyaltyPoints = userLoyaltyPoints;
    }

    public String getUserNickname() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname;
    }

    public String getEmail() {
        return userEmail;
    }

    public void setEmail(String email) {
        userEmail = email;
    }

    public String getPassword() {
        return userPassword;
    }

    public void setPassword(String password) {
        userPassword = password;
    }

    public Double getLoyaltyPoints() {
        return userLoyaltyPoints;
    }

    public void setLoyaltyPoints(Double loyaltyPoints) {
        userLoyaltyPoints = loyaltyPoints;
    }
}