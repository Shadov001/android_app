package com.example.engineer.dataBase.User;

import android.database.sqlite.SQLiteDatabase;

import com.example.engineer.dataBase.Game.GameTable;
import com.example.engineer.dataBase.Review.ReviewTable;

public class UserTable {

    public static final String USERS = "user_table";

    public static class UserColumns {
        public static final String ID = "Id";
        public static final String USER_EMAIL = "userEmail";
        public static final String USER_PASSWORD = "userPassword";
        public static final String USER_NICKNAME = "userNickname";
        public static final String USER_TYPE = "userType";
        public static final String USER_NAME = "userName";
        public static final String USER_SURNAME = "userSurname";
        public static final String USER_LOYALTYPOINTS = "userLoyaltyPoints";
    }

    public static void onCreate(SQLiteDatabase db) {
        String sql = "create table " + USERS + " ( " +
                UserColumns.ID + "  INTEGER PRIMARY KEY AUTOINCREMENT, " +
                UserColumns.USER_EMAIL + " TEXT, " +
                UserColumns.USER_PASSWORD + " TEXT, " +
                UserColumns.USER_NICKNAME + " TEXT, " +
                UserColumns.USER_TYPE + " INTEGER, " +
                UserColumns.USER_NAME + " TEXT, " +
                UserColumns.USER_SURNAME+ " TEXT, " +
                UserColumns.USER_LOYALTYPOINTS + " DOUBLE);";

        db.execSQL(sql);
    }


}
