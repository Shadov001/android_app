package com.example.engineer.dataBase.Favorite;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.media.Image;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import org.w3c.dom.Text;

import java.io.Serializable;

public class Favorite implements Serializable {
    private Integer Id;
    private String Title;
    private String Genre;
    private Integer Year;
    private String User;



    //constructor

    public Favorite(Integer id, String title, String genre, Integer year,  String user) {
        this.Id = id;
        this.Title = title;
        this.Genre = genre;
        this.Year = year;
        this.User = user;

    }

    public Favorite() {

    }

    public int getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getGenre() {
        return Genre;
    }

    public void setGenre(String genre) {
        Genre = genre;
    }

    public Integer getYear() {
        return Year;
    }

    public void setYear(Integer year) {
        Year = year;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getUser() {
        return User;
    }

    public void setUser(String user) {
        User = user;
    }
}