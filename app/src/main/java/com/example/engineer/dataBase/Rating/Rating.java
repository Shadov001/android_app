package com.example.engineer.dataBase.Rating;

import java.io.Serializable;

public class Rating implements Serializable {
    private Integer Id;
    private String user;
    private String game;
    private Double value;


    public Rating(Integer id, String user, String game, Double value) {
        Id = id;
        this.user = user;
        this.game = game;
        this.value = value;
    }

    public Rating() {
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
