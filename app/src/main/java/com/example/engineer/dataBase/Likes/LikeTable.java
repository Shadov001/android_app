package com.example.engineer.dataBase.Likes;

import android.database.sqlite.SQLiteDatabase;

public class LikeTable {

    public static final String LIKES = "likes_table";

    public static class LikeColumns {
        public static final String ID = "Id";
        public static final String USER = "user";
        public static final String GAME = "game";

    }

    public static void onCreate(SQLiteDatabase db){
        String sql = "create table " + LIKES + " ( " +
                LikeColumns.ID + "  INTEGER PRIMARY KEY AUTOINCREMENT, " +
                LikeColumns.USER + " INTEGER, " +
                LikeColumns.GAME+ " TEXT); ";

        db.execSQL(sql);
    }
}
