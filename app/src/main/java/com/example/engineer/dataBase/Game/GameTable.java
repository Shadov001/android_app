package com.example.engineer.dataBase.Game;

import android.database.sqlite.SQLiteDatabase;

public class GameTable {

    public static final String GAMES = "games_table";

    public static class GameColumns {
        public static final String ID = "Id";
        public static final String GAME_TITLE = "gameTitle";
        public static final String GAME_GENRE = "gameGenre";
        public static final String GAME_YEAR = "year";
        public static final String GAME_DESCRIPTION = "gameDescription";
        public static final String GAME_PC = "gamePC";
        public static final String GAME_PS3 = "gamePS3";
        public static final String GAME_PS4 = "gamePS4";
        public static final String GAME_XONE = "gameXONE";
        public static final String GAME_X360 = "gameX360";
        public static final String GAME_NINTENDO = "gameNINTENDO";
        public static final String LIKES_COUNT = "likesCount";
        public static final String IMAGE = "image";
        public static final String RATING = "rating";
        //public static final String DISLIKES_COUNT = "dislikesCount";
    }


    public static void onCreate(SQLiteDatabase db) {
        String sql = "create table " + GAMES + " ( " +
                GameColumns.ID + "  INTEGER PRIMARY KEY AUTOINCREMENT, " +
                GameColumns.GAME_TITLE + " TEXT, " +
                GameColumns.GAME_GENRE + " TEXT, " +
                GameColumns.GAME_YEAR + " INTEGER, " +
                GameColumns.GAME_DESCRIPTION + " TEXT, " +
                GameColumns.GAME_PC+ " INTEGER, " +
                GameColumns.GAME_PS3+ " INTEGER, " +
                GameColumns.GAME_PS4 + " INTEGER, " +
                GameColumns.GAME_XONE+ " INTEGER, " +
                GameColumns.GAME_X360 + " INTEGER, " +
                GameColumns.GAME_NINTENDO + " INTEGER, " +
                GameColumns.IMAGE + " BLOB, " +
                GameColumns.LIKES_COUNT + " INTEGER, " +
                GameColumns.RATING + " DOUBLE);";

        db.execSQL(sql);
    }
}