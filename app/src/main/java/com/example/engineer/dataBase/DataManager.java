package com.example.engineer.dataBase;


import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteTableLockedException;
import android.widget.Toast;

import com.example.engineer.dataBase.Favorite.FavoriteDAO;
import com.example.engineer.dataBase.Game.GameDAO;
import com.example.engineer.dataBase.Likes.LikeDAO;
import com.example.engineer.dataBase.Rating.RatingDAO;
import com.example.engineer.dataBase.Review.ReviewDAO;
import com.example.engineer.dataBase.User.User;
import com.example.engineer.dataBase.User.UserDAO;

import java.time.LocalDateTime;


public class DataManager {
    public SQLiteDatabase db;
    private FavoriteDAO favoriteDAO;
    private GameDAO gameDAO;
    private UserDAO userDAO;
    private ReviewDAO reviewDAO;
    private LikeDAO likeDAO;
    private RatingDAO ratingDAO;

    public DataManager(SQLiteDatabase db) {
        this.db = db;
        favoriteDAO = new FavoriteDAO(this.db);
        gameDAO = new GameDAO(this.db);
        userDAO = new UserDAO (this.db);
        reviewDAO = new ReviewDAO(this.db);
        likeDAO = new LikeDAO(this.db);
        ratingDAO = new RatingDAO(this.db);

    }

    public DataManager() {

    }

    public void addNewUser(String email,
                             String password,
                             String nickname,
                             Integer type,
                             String name,
                             String surname,
                             Double loyaltyPoints) {
        try {
            userDAO.save(new User(email, password, nickname, type, name, surname, loyaltyPoints));
            System.out.println("Registered");
        }
        catch(Exception e){
            System.out.println("Something went wrong.");
        }
    }


}
