package com.example.engineer.dataBase.Game;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.media.Image;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import org.w3c.dom.Text;

import java.io.Serializable;

public class Game implements Serializable {
    private int Id;
    private String Title;
    private String Type;
    private String Description;
    private Integer PCAvailable;
    private Integer PS3Available;
    private Integer PS4Available;
    private Integer XBOXAvailable;
    private Integer XBOXONEAvailable;
    private Integer NintendoAvailable;
    private Integer LikesCount;
    private Integer Year;
    private byte[] Image;
    private Double Rating;

    public Game(Integer id, String title, String type, Integer year, String description, Integer PCAvailable, Integer PS3Available, Integer PS4Available, Integer XBOXAvailable, Integer XBOXONEAvailable, Integer nintendoAvailable, Integer likesCount, byte[] image, Double rating) {
        this.Id = id;
        this.Title = title;
        this.Type = type;
        this.Description = description;
        this.PCAvailable = PCAvailable;
        this.PS3Available = PS3Available;
        this.PS4Available = PS4Available;
        this.XBOXAvailable = XBOXAvailable;
        this.XBOXONEAvailable = XBOXONEAvailable;
        this.NintendoAvailable = nintendoAvailable;
        this.LikesCount = likesCount;
        this.Year = year;
        this.Image = image;
        this.Rating = rating;
    }

    public Game() {

    }

    public Double getRating() {
        return Rating;
    }

    public void setRating(Double rating) {
        Rating = rating;
    }

    public Integer getYear() {
        return Year;
    }

    public void setYear(Integer year) {
        Year = year;
    }

    public Integer getLikesCount() {
        return LikesCount;
    }

    public void setLikesCount(Integer likesCount) {
        LikesCount = likesCount;
    }

    public void setPCAvailable(Integer PCAvailable) {
        this.PCAvailable = PCAvailable;
    }

    public void setPS3Available(Integer PS3Available) {
        this.PS3Available = PS3Available;
    }

    public void setPS4Available(Integer PS4Available) {
        this.PS4Available = PS4Available;
    }

    public void setXBOXAvailable(Integer XBOXAvailable) {
        this.XBOXAvailable = XBOXAvailable;
    }

    public void setXBOXONEAvailable(Integer XBOXONEAvailable) {
        this.XBOXONEAvailable = XBOXONEAvailable;
    }

    public void setNintendoAvailable(Integer nintendoAvailable) {
        NintendoAvailable = nintendoAvailable;
    }

    public byte[] getImage() {
        return Image;
    }

    public void setImage(byte[] image) {
        Image = image;
    }

    public Integer getPCAvailable() {
        return PCAvailable;
    }

    public Integer getPS3Available() {
        return PS3Available;
    }

    public Integer getPS4Available() {
        return PS4Available;
    }

    public Integer getXBOXAvailable() {
        return XBOXAvailable;
    }

    public Integer getXBOXONEAvailable() {
        return XBOXONEAvailable;
    }

    public Integer getNintendoAvailable() {
        return NintendoAvailable;
    }

    public Integer getId() {
        return Id;
    }

    public String getTitle()
    {
        return Title;
    }

    public String getType()
    {
        return Type;
    }

    public String getDescription()
    {
        return Description;
    }

    public void setId(Integer id) {
        Id = id;
    }


    public void setTitle(String title)
    {
        this.Title = title;
    }


    public void setType(String type)
    {
        this.Type = type;
    }



    public void setDescription(String description)
    {
        this.Description = description;
    }

    @NonNull
    @Override
    public String toString() {

        return Title + " - " + Type;
    }
}

