package com.example.engineer.dataBase.Review;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.example.engineer.dataBase.common.DAO;

import java.util.ArrayList;
import java.util.List;

public class ReviewDAO implements DAO<Review> {
    public SQLiteDatabase db;

    public ReviewDAO(SQLiteDatabase db) {
        this.db = db;
    }

    @Override
    public void save(Review review) {
        SQLiteStatement stmt = db.compileStatement( "insert into " + ReviewTable.REVIEWS + " ( " +
                //                ReviewTable.ReviewColumns.ID + ", " +
                ReviewTable.ReviewColumns.GAME + ", " +
                ReviewTable.ReviewColumns.AUTHOR + ", " +
                ReviewTable.ReviewColumns.AUTHOR_NAME + ", " +
                ReviewTable.ReviewColumns.CONTENT +
                " ) values ( \'" +
//                game.getId() + "\', \'" +
                review.getGame() + "\', \'" +
                review.getAuthor() + "\', \'" +
                review.getAuthorName() + "\', \'" +
                review.getReviewContent() +
                "\' )");
        stmt.execute();
        stmt.close();
    }


    public void updateReviewContent(String newReview, int reviewId) {
        SQLiteStatement stmt = db.compileStatement( "update " +
                ReviewTable.REVIEWS + " SET " +
                ReviewTable.ReviewColumns.CONTENT + " = \'" +
                newReview + "\' WHERE " +
                ReviewTable.ReviewColumns.ID + " = " +
                reviewId +";"
        );
        stmt.execute();
        stmt.close();
    }

    public List<Review> getAll() {
        List<Review> reviews = new ArrayList<>();

        String sql = "select * from " + ReviewTable.REVIEWS + ";";
        Cursor cursor = db.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            Review review = new Review();
            review.setId(cursor.getInt(0));
            review.setGame(cursor.getString(1));
            review.setAuthor(cursor.getString(2));
            review.setAuthorName(cursor.getString(3));
            review.setReviewContent(cursor.getString(4));
            review.setLikesCount(cursor.getInt(5));
            reviews.add(review);
        }

        return reviews;
    }

    public void addLike(Integer reviewId) {
        SQLiteStatement stmt = db.compileStatement( "update " +
                ReviewTable.REVIEWS + " SET " +
                ReviewTable.ReviewColumns.LIKES_COUNT + " = \'" +
                ReviewTable.ReviewColumns.LIKES_COUNT + " + 1 " +
                 "\' WHERE " +
                ReviewTable.ReviewColumns.ID + " = " +
                reviewId +";"
        );
        stmt.execute();
        stmt.close();
    }

}
