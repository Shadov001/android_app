package com.example.engineer.dataBase.Favorite;

import android.database.sqlite.SQLiteDatabase;

public class FavoriteTable {

    public static final String FAVORITES = "favorites_table";

    public static class FavoritesColumns {
        public static final String ID = "Id";
        public static final String TITLE = "title";
        public static final String GENRE = "genre";
        public static final String YEAR = "year";
        public static final String USER = "user";


       // public static final String FAVORITE_GAME_ID = "gameId";
    }

    public static void onCreate(SQLiteDatabase db){
        String sql = "create table " + FAVORITES + " ( " +
                FavoritesColumns.ID + "  INTEGER PRIMARY KEY AUTOINCREMENT, " +
                FavoritesColumns.TITLE + " TEXT, " +
                FavoritesColumns.GENRE + " TEXT, " +
                FavoritesColumns.YEAR + " INTEGER, " +
                FavoritesColumns.USER + " TEXT); ";

        db.execSQL(sql);
    }

}
