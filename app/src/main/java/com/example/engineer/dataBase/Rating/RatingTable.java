package com.example.engineer.dataBase.Rating;

import android.database.sqlite.SQLiteDatabase;

public class RatingTable {

    public static final String RATINGS = "ratings_table";

    public static class RatingColumns {
        public static final String ID = "Id";
        public static final String USER = "user";
        public static final String GAME = "game";
        public static final String VALUE = "value";


    }

    public static void onCreate(SQLiteDatabase db){
        String sql = "create table " + RATINGS + " ( " +
                RatingColumns.ID + "  INTEGER PRIMARY KEY AUTOINCREMENT, " +
                RatingColumns.USER + " TEXT, " +
                RatingColumns.GAME + " TEXT, " +
                RatingColumns.VALUE + " REAL); ";

        db.execSQL(sql);
    }
}
