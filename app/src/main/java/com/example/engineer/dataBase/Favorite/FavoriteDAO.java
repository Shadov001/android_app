package com.example.engineer.dataBase.Favorite;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.example.engineer.dataBase.common.DAO;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FavoriteDAO implements DAO<Favorite> {
    public SQLiteDatabase db;

    public FavoriteDAO(SQLiteDatabase db) { this.db = db;}

    public void save(Favorite favorite) {

        SQLiteStatement stmt = db.compileStatement( "insert into " + FavoriteTable.FAVORITES + " ( " +
                FavoriteTable.FavoritesColumns.TITLE +
                FavoriteTable.FavoritesColumns.USER +
                " ) values ( \'" +
                favorite.getTitle() +
                favorite.getUser() +
                "\' )");
        stmt.execute();
        stmt.close();
    }

    public List<Favorite> getAll() {
        List<Favorite> favorites = new ArrayList<>();

        String sql = "select * from " + FavoriteTable.FAVORITES + ";";
        Cursor cursor = db.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            Favorite favorite = new Favorite();
            favorite.setId(cursor.getInt(0));
            favorite.setUser(cursor.getString(1));
            favorite.setTitle(cursor.getString(2));
            favorites.add(favorite);
        }

        return favorites;
    }

//    public void updateDescription(String newDescription, int favoriteId) {
//        SQLiteStatement stmt = db.compileStatement("UPDATE " +
//                FavoriteTable.FAVORITES +" SET " +
//                FavoriteTable.FavoritesColumns.DESCRIPTION + " = \'" +
//                newDescription + "\' WHERE " +
//                FavoriteTable.FavoritesColumns.ID + " = " +
//                favoriteId +";"
//
//        );
//        stmt.execute();
//        stmt.close();
//    }

}
