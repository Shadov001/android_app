package com.example.engineer.dataBase.Review;

import android.app.DownloadManager;
import com.example.engineer.R;

import java.io.Serializable;

    public class Review implements Serializable{
        private Integer Id;
        private String Author;
        private String AuthorName;
        private String Game;
        private String ReviewContent;
        private Integer LikesCount;


        public Review(Integer id, String author, String authorName, String game, String reviewContent, Integer likesCount) {
            this.Id = id;
           this.Author = author;
           this.AuthorName = authorName;
           this.Game = game;
            this.ReviewContent = reviewContent;
            this.LikesCount = likesCount;
        }

        public Review(){

        }

        public Integer getLikesCount() {
            return LikesCount;
        }

        public void setLikesCount(Integer likesCount) {
            LikesCount = likesCount;
        }

        public String getAuthorName() {
            return AuthorName;
        }

        public void setAuthorName(String authorName) {
            AuthorName = authorName;
        }

        public String getGame() {
            return Game;
        }

        public void setGame(String game) {
            Game = game;
        }

        public void setReviewContent(String reviewContent) {
            this.ReviewContent = reviewContent;
        }


        public String getReviewContent() {
            return ReviewContent;
        }

        public Integer getId() {
            return Id;
        }

        public void setId(Integer id) {
            Id = id;
        }

        public String getAuthor() {
            return Author;
        }

        public void setAuthor(String author) {
            Author = author;
        }

    }




