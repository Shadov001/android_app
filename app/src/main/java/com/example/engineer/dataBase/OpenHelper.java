package com.example.engineer.dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.example.engineer.dataBase.Favorite.Favorite;
import com.example.engineer.dataBase.Favorite.FavoriteTable;
import com.example.engineer.dataBase.Game.GameTable;
import com.example.engineer.dataBase.Likes.LikeTable;
import com.example.engineer.dataBase.Rating.RatingTable;
import com.example.engineer.dataBase.Review.Review;
import com.example.engineer.dataBase.Review.ReviewTable;
import com.example.engineer.dataBase.User.UserTable;
import java.util.ArrayList;
import java.util.List;

public class OpenHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static String DB_NAME = "RateYourGame.db";

    public OpenHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        FavoriteTable.onCreate(db);
        GameTable.onCreate(db);
        ReviewTable.onCreate(db);
        UserTable.onCreate(db);
        LikeTable.onCreate(db);
        RatingTable.onCreate(db);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + FavoriteTable.FAVORITES);
        db.execSQL("DROP TABLE IF EXISTS " + GameTable.GAMES);
        db.execSQL("DROP TABLE IF EXISTS " + ReviewTable.REVIEWS);
        db.execSQL("DROP TABLE IF EXISTS " + UserTable.USERS);
        db.execSQL("DROP TABLE IF EXISTS " + LikeTable.LIKES);
        db.execSQL("DROP TABLE IF EXISTS " + RatingTable.RATINGS);
        onCreate(db);
    }

    public Boolean checkEmail(String email){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM user_table WHERE userEmail = ?",new String[]{email});
        if (cursor.getCount()>0) return false;
        else return true;
    }

    public Boolean checkIfGameExists(String title){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM games_table WHERE gameTitle = ?",new String[]{title});
        if (cursor.getCount()>0) return false;
        else return true;
    }

    public Boolean loginCheck(String email, String password){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM user_table WHERE userEmail=? and userPassword=?",new String[]{email,password});
        if (cursor.getCount()>0) return true;
        else return false;
    }


    public void insertReview(String game, String author,String name, String content){
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement( "insert into " + ReviewTable.REVIEWS + " ( " +
                //                ReviewTable.ReviewColumns.ID + ", " +
                ReviewTable.ReviewColumns.GAME + ", " +
                ReviewTable.ReviewColumns.AUTHOR + ", " +
                ReviewTable.ReviewColumns.AUTHOR_NAME + ", " +
                ReviewTable.ReviewColumns.CONTENT + ", " +
                ReviewTable.ReviewColumns.LIKES_COUNT +
                " ) values ( \'" +
//                game.getId() + "\', \'" +
                game + "\', \'" +
                author + "\', \'" +
                name + "\', \'" +
                content + "\', \'" +
                0 +
                "\' )");
        stmt.execute();
        stmt.close();
    }

    public Boolean checkIfInFavorites(String user, String game){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + FavoriteTable.FAVORITES + " WHERE user=? and title=?",new String[]{user,game});
        if (cursor.getCount()>0) return true;
        else return false;
    }

    public void addToFavorites(String user, String game, String genre, Integer year){
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement( "insert into " + FavoriteTable.FAVORITES + " ( " +
                //                GameTable.GameColumns.ID + ", " +
                FavoriteTable.FavoritesColumns.USER + ", " +
                FavoriteTable.FavoritesColumns.GENRE + ", " +
                FavoriteTable.FavoritesColumns.YEAR + ", " +
                FavoriteTable.FavoritesColumns.TITLE +
                " ) values ( \'" +
//                game.getId() + "\', \'" +
                user + "\', \'" +
                genre + "\', \'" +
                year + "\', \'" +
                game +
                "\' )");
        stmt.execute();
        stmt.close();
    }

    public void removeFromFavorites(String user, String game){
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement( "delete from " + FavoriteTable.FAVORITES +
                        " WHERE " +
                        FavoriteTable.FavoritesColumns.USER + " = '" +
                        user + "'" + " and " +
                        FavoriteTable.FavoritesColumns.TITLE + " = '" +
                        game + "'" + ";"
                        );
        stmt.execute();
        stmt.close();
    }

    public Cursor getFavoritesByUser(String user) {
        SQLiteDatabase db = this.getReadableDatabase();

        String sql = "select * from " + FavoriteTable.FAVORITES + " where " +
                FavoriteTable.FavoritesColumns.USER + " = \'" +
                user + "\' "
                + ";";
        Cursor cursor = db.rawQuery(sql, null);
        return cursor;
    }

    public Cursor getReviewsByGame(String game) {
        SQLiteDatabase db = this.getReadableDatabase();

        String sql = "select * from " + ReviewTable.REVIEWS + " where " +
                ReviewTable.ReviewColumns.GAME + " = \'" +
                game + "\' "
                + ";";
        Cursor cursor = db.rawQuery(sql, null);
        return cursor;
    }

    public Cursor getGames() {
        SQLiteDatabase db = this.getReadableDatabase();

        String sql = "select * from " + GameTable.GAMES
                + ";";
        Cursor cursor = db.rawQuery(sql, null);
        return cursor;
    }

    public void addGame(String title, String genre, Integer year, String description, int pcAvailable, int ps3Available, int ps4Available, int xoneAvailable, int x360Available, int nintendoAvailable, byte[] image ){
        SQLiteDatabase db = this.getWritableDatabase();

        String sql = "INSERT INTO games_table VALUES (null, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0, 0)";

        SQLiteStatement stmt = db.compileStatement(sql);
        stmt.clearBindings();

        stmt.bindString(1, title);
        stmt.bindString(2, genre);
        stmt.bindDouble(3, year);
        stmt.bindString(4, description);
        stmt.bindLong(5, pcAvailable);
        stmt.bindLong(6, ps3Available);
        stmt.bindLong(7, ps4Available);
        stmt.bindLong(8, xoneAvailable);
        stmt.bindLong(9, x360Available);
        stmt.bindLong(10, nintendoAvailable);
        stmt.bindBlob(11, image);

        stmt.execute();
        stmt.close();
        //System.out.println("długość: " + image.length);
    }


    public Boolean checkIfLiked(String title, String user){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM likes_table WHERE game = ? and user = ?",new String[]{title, user});
        if (cursor.getCount()>0) return true;
        else return false;
    }

    public void likeGame(String title, String user){
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement( "insert into " + LikeTable.LIKES + " ( " +
                LikeTable.LikeColumns.USER + ", " +
                LikeTable.LikeColumns.GAME+
                " ) values ( \'" +
                user + "\', \'" +
                title +
                "\' )");
        stmt.execute();
        stmt.close();
    }

    public void removeLike(String title, String user){
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement( "delete from " + LikeTable.LIKES +
                " WHERE " +
                LikeTable.LikeColumns.USER  + " = '" +
                user + "'" + " and " +
                LikeTable.LikeColumns.GAME + " = '" +
                title + "'" + ";"
        );
        stmt.execute();
        stmt.close();
    }

    public Integer countLikes(String gameTitle){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCount= db.rawQuery("select count(*) from likes_table where game='" + gameTitle +"'", null);
        mCount.moveToFirst();
        int count= mCount.getInt(0);
        mCount.close();

        return count;
    }

    public String getAuthorNameByEmail(String email){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCount= db.rawQuery("select userName from user_table where userEmail='" + email +"'", null);
        mCount.moveToFirst();
        String name= mCount.getString(0);
        return name;
    }

    public String getUserNameByEmail(String email){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCount= db.rawQuery("select userName from user_table where userEmail='" + email +"'", null);
        mCount.moveToFirst();
        String name= mCount.getString(0);
        return name;
    }

    public Boolean checkIfNicknameInUse(String nickname){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT userNickname FROM user_table WHERE userNickname = ?",new String[]{nickname});
        if (cursor.getCount()>0) return true;
        else return false;
    }

    public String getNicknameByEmail(String email){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCount= db.rawQuery("select userNickname from user_table where userEmail='" + email +"'", null);
        mCount.moveToFirst();
        String nickname= mCount.getString(0);
        return nickname;
    }

    public void addRating(String user, String game, Double value){
        SQLiteDatabase db = this.getWritableDatabase();

        String sql = "INSERT INTO ratings_table VALUES (null, ?, ?, ?)";

        SQLiteStatement stmt = db.compileStatement(sql);
        stmt.clearBindings();

        stmt.bindString(1, user);
        stmt.bindString(2, game);
        stmt.bindDouble(3, value);

        stmt.execute();
        stmt.close();
    }

    public boolean checkIfUserRated(String game, String user){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM ratings_table WHERE game = ? and user = ?",new String[]{game, user});
        if (cursor.getCount()>0) return true;
        else return false;
    }

    public Float getRate(String game, String user){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCount= db.rawQuery("select value from ratings_table where user = ? and game = ?", new String[]{user, game});
        mCount.moveToFirst();
        Float rating = mCount.getFloat(0);
        return rating;
    }


    public void updateUserRating(String game, String user, Double newValue){
        SQLiteDatabase db = this.getWritableDatabase();

        String sql = "UPDATE ratings_table SET value = ? WHERE game = ? and user = ?";

        SQLiteStatement stmt = db.compileStatement(sql);
        stmt.clearBindings();

        stmt.bindDouble(1, newValue);
        stmt.bindString(2, game);
        stmt.bindString(3, user);

        stmt.execute();
        stmt.close();
    }

    public Double avgRating(String game){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor1 = db.rawQuery("SELECT * FROM favorites_table WHERE title = ?",new String[]{game});
        int counter = cursor1.getCount();

        Cursor cursor2 = db.rawQuery("SELECT avg(value) FROM ratings_table WHERE game = ?", new String[]{game});
        cursor2.moveToFirst();
        double average = cursor2.getDouble(0);

        if(counter == 0){
            return average * 1.0;
        }
        else {
            Double value = average * ((counter/100.0)+1.0);
            if (value > 5){
                return 5.0;
            }
            else{
                return value;
            }

        }
    }


    public void passAvgRatingToGame(String game, Double avg){
        SQLiteDatabase db = this.getWritableDatabase();

        String sql = "UPDATE games_table SET rating = ? WHERE gameTitle = ?";
        SQLiteStatement stmt = db.compileStatement(sql);
        stmt.clearBindings();

        stmt.bindDouble(1, avg);
        stmt.bindString(2, game);

        stmt.execute();
        stmt.close();
    }

    public Double getRating(String game){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT rating FROM games_table WHERE gameTitle = ?",new String[]{game});
        cursor.moveToFirst();
        Double average = cursor.getDouble(0);
        return average;
    }

    public Cursor getRanking() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from  games_table order by rating desc", null);
        return cursor;
    }

    public Integer getReviewsForUser(String user){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM Reviews WHERE AUTHOR = ?",new String[]{user});
        Integer counter = cursor.getCount();
        return counter;
    }

    public Integer getRatesCounterForGame(String game){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM ratings_table WHERE game = ?",new String[]{game});
        Integer counter = cursor.getCount();
        return counter;
    }

    public Integer getFavoritesCounterForGame(String game){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM favorites_table WHERE title = ?",new String[]{game});
        Integer counter = cursor.getCount();
        return counter;

    }

}
