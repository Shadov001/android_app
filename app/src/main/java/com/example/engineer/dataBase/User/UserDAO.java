package com.example.engineer.dataBase.User;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.example.engineer.dataBase.Review.Review;
import com.example.engineer.dataBase.Review.ReviewTable;
import com.example.engineer.dataBase.common.DAO;

import java.util.ArrayList;
import java.util.List;

public class UserDAO implements DAO<User>  {
    public SQLiteDatabase db;

    public UserDAO(SQLiteDatabase db) {
        this.db = db;
    }



    @Override
    public void save(User user) {
        SQLiteStatement stmt = db.compileStatement( "insert into " + UserTable.USERS + " ( " +
                //                GameTable.GameColumns.ID + ", " +
                UserTable.UserColumns.USER_EMAIL + ", " +
                UserTable.UserColumns.USER_PASSWORD + ", " +
                UserTable.UserColumns.USER_NICKNAME + ", " +
                UserTable.UserColumns.USER_TYPE + ", " +
                UserTable.UserColumns.USER_NAME + ", " +
                UserTable.UserColumns.USER_SURNAME +

                " ) values ( \'" +
//                game.getId() + "\', \'" +
                user.getEmail() + "\', \'" +
                user.getPassword() + "\', \'" +
                user.getUserNickname() + "\', \'" +
                user.getUserType() + "\', \'" +
                user.getName() + "\', \'" +
                user.getSurname() +
                "\' )");
        stmt.execute();
        stmt.close();
    }

    public List<User> getAll() {
        List<User> users = new ArrayList<>();

        String sql = "select * from " + UserTable.USERS + ";";
        Cursor cursor = db.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            User user = new User();
            user.setId(cursor.getInt(0));
            user.setEmail(cursor.getString(1));
            user.setPassword(cursor.getString(2));
            user.setUserNickname(cursor.getString(3));
            user.setUserType(cursor.getInt(4));
            user.setName(cursor.getString(5));
            user.setSurname(cursor.getString(6));
            user.setLoyaltyPoints(cursor.getDouble(7));



            users.add(user);
        }

        return users;
    }

     public void updateLoyaltyPoints(Integer userId){
         // to do : ponakładaj różne mnożniki
        SQLiteStatement stmt = db.compileStatement( "update " +
                 UserTable.USERS + " SET " +
                 UserTable.UserColumns. USER_LOYALTYPOINTS + " = \'" +
                 UserTable.UserColumns. USER_LOYALTYPOINTS + " + 2 " +
                 "\' WHERE " +
                 UserTable.UserColumns.ID + " = " +
                 userId +";"
         );
         stmt.execute();
         stmt.close();
     }



}
