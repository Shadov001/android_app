package com.example.engineer.dataBase.common;

import java.util.List;

public interface DAO<T> {
    void save(T t);
    //void saveFavorite(T t);
    List<T> getAll();
}

