package com.example.engineer.Repository.Encyclopedia;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.engineer.Repository.Reviews.GameReviewsActivity;
import com.example.engineer.dataBase.DataManager;
import com.example.engineer.dataBase.Game.Game;
import com.example.engineer.R;
import com.example.engineer.Repository.Rate.RateActivity;
import com.example.engineer.dataBase.OpenHelper;
import com.example.engineer.dataBase.User.User;

import org.w3c.dom.Text;

public class DetailGameActivity extends AppCompatActivity {
    private Game selectedGame;
    private TextView txtTitle;
    private TextView txtDescription;
    private String loggedUser;
    private ImageButton btnLike;
    private ImageButton btnDislike;
    private TextView likesCounter;
    private TextView dislikesCounter;
    private TextView gameYear;
    private Integer gameId;
    private DataManager dataManager;
    private Boolean isAlreadyLiked;
    private Integer pcAvailable;
    private Integer ps4Available;
    private Integer ps3Available;
    private Integer xboxOneAvailable;
    private Integer xbox360Available;
    private Integer nintendoAvailable;
    private ImageButton btnAddToFavorites;
    private String gameTitle;
    private Boolean checkIfInFavorites;
    private Boolean checkIfLiked;
    private ImageView imagePC;
    private ImageView imagePS3;
    private ImageView imagePS4;
    private ImageView imageX360;
    private ImageView imageXONE;
    private ImageView imageNINTENDO;
    private String gameGenre;
    private Integer gameYearAttr;
    private Integer likes;
    private Integer likesUpdated;
    private Integer reviewsForUser;
    private byte[] gameImage;
    private ImageView image;
    RatingBar ratingBar;
    private Boolean checkIfUserRated;
    private TextView textRateValue;
    OpenHelper db;
    private Button btnShowRate;
    private Double rating;
    private Double multipliedRating;
    private Integer favoritesCounter1;
    private Integer favoritesCounter2;
    private TextView textAverage, textYourRating, textAllRatings, textProductionYear;
    private Integer allRates;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_game);

        dataManager = new DataManager();
        db = new OpenHelper(this);

        selectedGame = (Game)getIntent().getExtras().getSerializable("selectedGame");
        loggedUser = getIntent().getExtras().getString("loggedUser");
       // checkIfInFavorites = getIntent().getExtras().getBoolean("favoriteStatus");
        gameImage = getIntent().getExtras().getByteArray("gameImage");
        //checkIfLiked = getIntent().getExtras().getBoolean("checkIfLiked");


        gameId = selectedGame.getId();
        gameTitle = selectedGame.getTitle();
        gameGenre = selectedGame.getType();
        gameYearAttr = selectedGame.getYear();
        pcAvailable = selectedGame.getPCAvailable();
        ps4Available = selectedGame.getPS4Available();
        ps3Available = selectedGame.getPS3Available();
        xbox360Available = selectedGame.getXBOXAvailable();
        xboxOneAvailable = selectedGame.getXBOXONEAvailable();
        nintendoAvailable = selectedGame.getNintendoAvailable();

        imagePC = findViewById(R.id.imagePC);
        imagePS3 = findViewById(R.id.imagePS3);
        imagePS4 = findViewById(R.id.imagePS4);
        imageX360 = findViewById(R.id.imageX360);
        imageXONE = findViewById(R.id.imageXONE);
        imageNINTENDO = findViewById(R.id.imageNINTENDO);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        textAverage = (TextView) findViewById(R.id.textAverage);
        textYourRating = (TextView) findViewById(R.id.textYourRating);
        textAllRatings = (TextView) findViewById(R.id.textAllRatings);
        textProductionYear = (TextView) findViewById(R.id.textProductionYear);
        textProductionYear.setText(getResources().getString(R.string.label_year));


        rating = db.avgRating(gameTitle);
        textAverage.setText(getResources().getString(R.string.label_overall_rating) + ": " + String.format("%.2f", rating));

        allRates = db.getRatesCounterForGame(gameTitle);
        textAllRatings.setText(getResources().getString(R.string.text_all_ratings) + ": " + String.valueOf(allRates));

        checkIfUserRated = db.checkIfUserRated(gameTitle, loggedUser);
        if (checkIfUserRated) {
            Float rate = db.getRate(gameTitle, loggedUser);
            ratingBar.setRating(rate);
            textYourRating.setText(getResources().getString(R.string.label_your_rating) + ": " + rate);
        } else {
            ratingBar.setRating(5);
        }

        reviewsForUser = db.getReviewsForUser(loggedUser);
        System.out.println(reviewsForUser);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                checkIfUserRated = db.checkIfUserRated(gameTitle, loggedUser);
                if (checkIfUserRated) {
                    ratingBar.setRating(rating);
                    if(reviewsForUser >= 10 && reviewsForUser < 20){
                        multipliedRating = rating * 1.1;
                    }
                    else if(reviewsForUser >= 20 && reviewsForUser <= 30){
                        multipliedRating = rating * 1.15;
                    }
                    else if(reviewsForUser > 30){
                        multipliedRating = rating * 1.2;
                    }
                    else {
                        multipliedRating = rating * 1.1/1.1;
                    }
                        db.updateUserRating(gameTitle, loggedUser, multipliedRating);
                        textYourRating.setText(getResources().getString(R.string.label_your_rating) + ": " + rating);
                        Double average = db.avgRating(gameTitle);
                        db.passAvgRatingToGame(gameTitle, average);
                        Double updatedRating = db.getRating(gameTitle);
                        textAverage.setText(getResources().getString(R.string.label_overall_rating) + ": " + String.format("%.2f", updatedRating));
                        allRates = db.getRatesCounterForGame(gameTitle);
                        textAllRatings.setText(getResources().getString(R.string.text_all_ratings) + ": " + String.valueOf(allRates));


                } else {
                    if(reviewsForUser >= 10 && reviewsForUser < 20){
                        multipliedRating = rating * 1.1;
                    }
                    else if(reviewsForUser >= 20 && reviewsForUser <= 30){
                        multipliedRating = rating * 1.15;
                    }
                    else if(reviewsForUser > 30){
                        multipliedRating = rating * 1.2;
                    }
                    else {
                        multipliedRating = rating * 1.1/1.1;
                    }
                    db.addRating(loggedUser, gameTitle, multipliedRating);
                    textYourRating.setText(getResources().getString(R.string.label_your_rating) + ": " + rating);
                    Double average = db.avgRating(gameTitle);
                    db.passAvgRatingToGame(gameTitle, average);
                    Double updatedRating = db.getRating(gameTitle);
                    textAverage.setText(getResources().getString(R.string.label_overall_rating) + ": " + String.format("%.2f", updatedRating));
                    allRates = db.getRatesCounterForGame(gameTitle);
                    textAllRatings.setText(getResources().getString(R.string.text_all_ratings) + ": " + String.valueOf(allRates));
                }

            }
        });

        image = findViewById(R.id.gameImage);
        Bitmap bitmap = BitmapFactory.decodeByteArray(gameImage, 0, gameImage.length);
        image.setImageBitmap(bitmap);

        gameYear = findViewById(R.id.textYear);
        gameYear.setText(String.valueOf(selectedGame.getYear()));

        if(pcAvailable == 2){
            imagePC.setBackgroundResource(R.drawable.check_circle_red);
        }
        else{
            imagePC.setBackgroundResource(R.drawable.check_circle_grey);
        }

        if(ps3Available == 2){
            imagePS3.setBackgroundResource(R.drawable.check_circle_red);
        }
        else{
            imagePS3.setBackgroundResource(R.drawable.check_circle_grey);
        }

        if(ps4Available == 2){
            imagePS4.setBackgroundResource(R.drawable.check_circle_red);
        }
        else{
            imagePS4.setBackgroundResource(R.drawable.check_circle_grey);
        }

        if(xbox360Available == 2){
            imageX360.setBackgroundResource(R.drawable.check_circle_red);
        }
        else{
            imageX360.setBackgroundResource(R.drawable.check_circle_grey);
        }

        if(xboxOneAvailable == 2){
            imageXONE.setBackgroundResource(R.drawable.check_circle_red);
        }
        else{
            imageXONE.setBackgroundResource(R.drawable.check_circle_grey);
        }

        if(nintendoAvailable == 2){
            imageNINTENDO.setBackgroundResource(R.drawable.check_circle_red);
        }
        else{
            imageNINTENDO.setBackgroundResource(R.drawable.check_circle_grey);
        }


        likes = db.countLikes(gameTitle);
        likesCounter = (TextView) findViewById(R.id.likesCounter);
        likesCounter.setText(String.valueOf(likes));

        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtTitle.setText(selectedGame.getTitle());

        txtDescription = (TextView) findViewById(R.id.txtDescription);
        txtDescription.setText(selectedGame.getDescription());

        btnLike = findViewById(R.id.btnLike);
        btnLike.setBackgroundResource(R.drawable.ic_thumb_up_grey);

        checkIfLiked = db.checkIfLiked(gameTitle, loggedUser);
        if(checkIfLiked){
            btnLike.setBackgroundResource(R.drawable.thumb_up);
        }
        else{
            btnLike.setBackgroundResource(R.drawable.ic_thumb_up_grey);
        }


        btnLike.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                if(checkIfLiked == false) {
                    db.likeGame(gameTitle, loggedUser);
                    likesUpdated = db.countLikes(gameTitle);
                    likesCounter.setText(String.valueOf(likesUpdated));
                    btnLike.setBackgroundResource(R.drawable.thumb_up);
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.action_like), Toast.LENGTH_SHORT).show();
                    checkIfLiked = db.checkIfLiked(gameTitle, loggedUser);
                }

           else{
                    db.removeLike(gameTitle, loggedUser);
                    likesUpdated = db.countLikes(gameTitle);
                    likesCounter.setText(String.valueOf(likesUpdated));
                    btnLike.setBackgroundResource(R.drawable.ic_thumb_up_grey);
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.action_dislike), Toast.LENGTH_SHORT).show();
                    checkIfLiked = db.checkIfLiked(gameTitle, loggedUser);

                }


                System.out.println("Likes: " + selectedGame.getLikesCount());
            }
        });




        checkIfInFavorites = db.checkIfInFavorites(loggedUser,gameTitle);
        btnAddToFavorites = findViewById(R.id.btnAddToFavorites);


        favoritesCounter1 = db.getFavoritesCounterForGame(gameTitle);

        if(checkIfInFavorites){
            btnAddToFavorites.setBackgroundResource(R.drawable.ic_grade_gold);
        }
        else{
            btnAddToFavorites.setBackgroundResource(R.drawable.ic_grade_black_24dp);
        }

        btnAddToFavorites.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(checkIfInFavorites){
                        db.removeFromFavorites(loggedUser, gameTitle);
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.action_delete_from_favorites), Toast.LENGTH_SHORT).show();
                        btnAddToFavorites.setBackgroundResource(R.drawable.ic_grade_black_24dp);

                    Double avg= db.avgRating(gameTitle);
                    db.passAvgRatingToGame(gameTitle, avg);
                    Double updated = db.getRating(gameTitle);
                    textAverage.setText(getResources().getString(R.string.label_overall_rating) + ": " + String.format("%.2f", updated));
                    checkIfInFavorites = db.checkIfInFavorites(loggedUser,gameTitle);


                }
                else {
                        db.addToFavorites(loggedUser, gameTitle, gameGenre, gameYearAttr);
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.action_add_to_favorites), Toast.LENGTH_SHORT).show();
                        btnAddToFavorites.setBackgroundResource(R.drawable.ic_grade_gold);

                    Double avg = db.avgRating(gameTitle);
                    db.passAvgRatingToGame(gameTitle, avg);
                    Double updated = db.getRating(gameTitle);
                    textAverage.setText(getResources().getString(R.string.label_overall_rating) + ": " + String.format("%.2f", updated));
                    checkIfInFavorites = db.checkIfInFavorites(loggedUser,gameTitle);

                }

                System.out.println(checkIfInFavorites);
            }
        });

        Button button = findViewById(R.id.rateButton);
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                navigateToRateActivity();
            }
        });

        Button button2 = findViewById(R.id.allReviewsButton);
        button2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                navigateToAllReviewsActivity();
            }
        });


    }

    public void navigateToRateActivity(){
        Intent intent = new Intent(DetailGameActivity.this, RateActivity.class);
        intent.putExtra("selectedGame",  selectedGame);
        intent.putExtra("loggedUser",  loggedUser);
        startActivity(intent);
    }

    public void navigateToAllReviewsActivity(){
        Intent intent = new Intent(DetailGameActivity.this, GameReviewsActivity.class);
        intent.putExtra("selectedGame",  selectedGame);
        intent.putExtra("loggedUser",  loggedUser);
        startActivity(intent);
    }

}
