package com.example.engineer.Repository.Rankings;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.engineer.R;
import com.example.engineer.Repository.Encyclopedia.GameItemView;
import com.example.engineer.Repository.Encyclopedia.GamesActivity;

public class RankingItemView extends RecyclerView.ViewHolder {
    public TextView txtTitle;
    public TextView txtType;
    public TextView txtDescription;
    public TextView txtRating;
    public TextView txtTop;
    ImageView imageGame;
    private RankingActivity RankingActivity;

    public void setRankingActivity(RankingActivity rankingActivity) {
        this.RankingActivity = rankingActivity;

    }

    public RankingItemView(View view){
        super(view);
        txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        txtType = (TextView) view.findViewById(R.id.txtType);
        imageGame = (ImageView) view.findViewById(R.id.imageGame);
        txtDescription = (TextView) view.findViewById(R.id.textDesc);
        txtRating = (TextView) view.findViewById(R.id.txtRating);
        txtTop = (TextView) view.findViewById(R.id.txtTop);

    }
}
