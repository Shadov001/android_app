package com.example.engineer.Repository.Favorites;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.engineer.R;
import com.example.engineer.Repository.Encyclopedia.DetailGameActivity;
import com.example.engineer.Repository.Encyclopedia.GamesActivity;
import com.example.engineer.Repository.Reviews.ReviewListAdapter;
import com.example.engineer.dataBase.Favorite.Favorite;
import com.example.engineer.dataBase.Game.Game;
import com.example.engineer.dataBase.OpenHelper;
import com.example.engineer.dataBase.Review.Review;

import java.util.ArrayList;

public class FavoritesActivity extends AppCompatActivity {

    private String loggedUser;
    OpenHelper db;

    ArrayList<Favorite> listItem;
    ArrayAdapter adapter;
    ListView favoritesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        db = new OpenHelper(this);
        loggedUser = getIntent().getExtras().getString("loggedUser");
        favoritesList = findViewById(R.id.favoritesList);
        listItem = new ArrayList<>();

        Cursor cursor = db.getFavoritesByUser(loggedUser);
        if(cursor.getCount() == 0){
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.info_no_favorites), Toast.LENGTH_SHORT).show();
        }
        else {
            while (cursor.moveToNext()) {
                listItem.add(new Favorite(cursor.getInt(cursor.getColumnIndex("Id")), cursor.getString(cursor.getColumnIndex("title")),  cursor.getString(cursor.getColumnIndex("genre")), cursor.getInt(cursor.getColumnIndex("year")),cursor.getString(cursor.getColumnIndex("user"))));
            }
        }
        final FavoriteListAdapter adapter = new FavoriteListAdapter(this, R.layout.favorite_row, listItem);
        favoritesList.setAdapter(adapter);


        favoritesList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
              //  final String selectedFavoriteTitle = favoritesList.getItemAtPosition(position).
                Favorite favorite = (Favorite)parent.getItemAtPosition(position);
                final String title = favorite.getTitle(); // third column in db
                AlertDialog.Builder adb = new AlertDialog.Builder(FavoritesActivity.this);
                // adb.setTitle("Delete?");
                adb.setMessage(getResources().getString(R.string.action_confirm1) + " " + title + " " + getResources().getString(R.string.action_confirm2));
                final int positionToRemove = position;
                adb.setNegativeButton(getResources().getString(R.string.action_cancel), null);
                adb.setPositiveButton(getResources().getString(R.string.action_yes), new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        db.removeFromFavorites(loggedUser, title);
                        Double avg= db.avgRating(title);
                        db.passAvgRatingToGame(title, avg);
                        listItem.remove(positionToRemove);
                        adapter.notifyDataSetChanged();
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.action_delete_from_favorites), Toast.LENGTH_SHORT).show();
                    }
                });
                adb.show();
                return false;
            }
        });

        favoritesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

    }

}
