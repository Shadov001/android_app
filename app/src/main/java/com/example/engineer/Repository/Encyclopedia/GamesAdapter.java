package com.example.engineer.Repository.Encyclopedia;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.engineer.dataBase.Game.Game;
import com.example.engineer.R;
import java.util.ArrayList;

public class GamesAdapter extends RecyclerView.Adapter<GameItemView> {

    private ArrayList<Game> gamesData;
    private GamesActivity gamesActivity;
    //constructor
    GamesAdapter(ArrayList<Game> gamesData){

        this.gamesData = gamesData;
    }

    public void setGamesActivity(GamesActivity gamesActivity) {
        this.gamesActivity = gamesActivity;
    }

    @NonNull
    @Override
    public GameItemView onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.game_item_view, viewGroup, false);
        GameItemView gameItemView = new GameItemView(view);
        gameItemView.setGamesActivity(this.gamesActivity);
        return gameItemView;
    }

    @Override
    public void onBindViewHolder(@NonNull GameItemView gameItemView, int i) {
        Game selectedGame = gamesData.get(i);
        String title = selectedGame.getTitle();
        String type = selectedGame.getType();
        gameItemView.txtTitle.setText(title);
        gameItemView.txtType.setText(type);

        byte[] image = selectedGame.getImage();
        Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
        gameItemView.imageGame.setImageBitmap(bitmap);
        //gameItemView.imageGame.setImageBitmap(Bitmap.createScaledBitmap(bitmap, gameItemView.imageGame.getWidth(), gameItemView.imageGame.getHeight(), false));

    }

    @Override
    public int getItemCount() {
        return gamesData.size();
    }
}