package com.example.engineer.Repository.Rankings;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.engineer.R;
import com.example.engineer.Repository.Encyclopedia.GameItemView;
import com.example.engineer.Repository.Encyclopedia.GamesActivity;
import com.example.engineer.dataBase.Game.Game;

import java.util.ArrayList;

public class RankingAdapter extends RecyclerView.Adapter<RankingItemView>{
    private ArrayList<Game> gamesData;
    private RankingActivity RankingActivity;

    RankingAdapter(ArrayList<Game> gamesData){

        this.gamesData = gamesData;
    }

    public void setRankingActivity(RankingActivity rankingActivity) {
        this.RankingActivity = rankingActivity;
    }

    @NonNull
    @Override
    public RankingItemView onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.ranking_item_view, viewGroup, false);
        RankingItemView rankingItemView = new RankingItemView(view);
        rankingItemView.setRankingActivity(this.RankingActivity);
        return rankingItemView;
    }

    @Override
    public void onBindViewHolder(@NonNull RankingItemView rankingItemView, int i) {
        Game selectedGame = gamesData.get(i);
        String title = selectedGame.getTitle();
        String type = selectedGame.getType();
        Double rating = selectedGame.getRating();
        String description = selectedGame.getDescription();
        rankingItemView.txtTitle.setText(title);
        rankingItemView.txtType.setText(type);
        rankingItemView.txtRating.setText(RankingActivity.getResources().getString(R.string.text_rating)+ ": " + String.format("%.2f", rating));
        rankingItemView.txtDescription.setText(description);
        Integer position = i + 1;
        rankingItemView.txtTop.setText("TOP " + position);
        byte[] image = selectedGame.getImage();
        Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
        rankingItemView.imageGame.setImageBitmap(bitmap);

    }

    @Override
    public int getItemCount() {
        return gamesData.size();
    }
}
