package com.example.engineer.Repository.Rate;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.engineer.dataBase.Game.Game;
import com.example.engineer.R;
import com.example.engineer.dataBase.OpenHelper;
import com.example.engineer.dataBase.User.User;


public class RateActivity extends Activity {
    private Game selectedGame;
    private TextView txtTitle;
    private EditText reviewText;
    private Button saveReviewBtn;
    private String loggedUser;
    private String loggedUserName;

   OpenHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate);
        db = new OpenHelper(this);
        selectedGame = (Game)getIntent().getExtras().getSerializable("selectedGame");
        loggedUser = getIntent().getExtras().getString("loggedUser");
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtTitle.setText(selectedGame.getTitle());
        reviewText = (EditText) findViewById(R.id.reviewText);
        saveReviewBtn = (Button) findViewById(R.id.saveReview);
        loggedUserName = db.getNicknameByEmail(loggedUser);

        saveReviewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                            db.insertReview(txtTitle.getText().toString(),
                                    loggedUser,
                                    loggedUserName,
                                    reviewText.getText().toString()
                                    );
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.action_insert_review_success), Toast.LENGTH_SHORT).show();
                            reviewText.setText("");

            }
        });

    }

}
