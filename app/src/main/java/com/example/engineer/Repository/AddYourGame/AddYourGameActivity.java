package com.example.engineer.Repository.AddYourGame;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.example.engineer.R;
import com.example.engineer.dataBase.DataManager;
import com.example.engineer.dataBase.OpenHelper;

import java.io.ByteArrayOutputStream;
import java.io.Externalizable;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class AddYourGameActivity extends AppCompatActivity {
    private OpenHelper openHelper;
    private SQLiteDatabase db;
    private DataManager dataManager;
    private Integer pcAvailable;
    private Integer ps3Available;
    private Integer ps4Available;
    private Integer x360Available;
    private Integer xoneAvailable;
    private Integer nintendoAvailable;
    final int REQUEST_CODE_GALLERY = 999;
    private Boolean isValidYear = false;
    private String yearPattern = "(19|2[0-9])[0-9]{2}";
    EditText textTitle, textGenre, textYear, textDescription;
    ImageView image;
    Button btnAddYourGame, btnAddImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_your_game);

        openHelper = new OpenHelper(this);
        db = openHelper.getWritableDatabase();
        dataManager = new DataManager(db);

        textTitle = findViewById(R.id.textTitle);
        textGenre = findViewById(R.id.textGenre);
        textYear = findViewById(R.id.textYear);
        textDescription = findViewById(R.id.textDescription);
        image = findViewById(R.id.gameImageView);

        btnAddYourGame = findViewById(R.id.btnAddYourGame);
        btnAddImage = findViewById(R.id.btnAddImage);


        final Switch switchPC = (Switch) findViewById(R.id.switchPC);
        final Switch switchPS3 = (Switch) findViewById(R.id.switchPS3);
        final Switch switchPS4 = (Switch) findViewById(R.id.switchPS4);
        final Switch switchX360 = (Switch) findViewById(R.id.switchX360);
        final Switch switchXONE = (Switch) findViewById(R.id.switchXONE);
        final Switch switchNINTENDO = (Switch) findViewById(R.id.switchNINTENDO);


        pcAvailable = 1;
        ps3Available = 1;
        ps4Available = 1;
        x360Available = 1;
        xoneAvailable = 1;
        nintendoAvailable = 1;

        switchPC.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    pcAvailable = 2;
                }
                else{
                    pcAvailable = 1;
                }
                System.out.println(pcAvailable);
            }
        });

        switchPS3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    ps3Available = 2;
                }
                else{
                    ps3Available = 1;
                }
            }
        });

        switchPS4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    ps4Available = 2;
                }
                else{
                    ps4Available = 1;
                }
            }
        });

        switchX360.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    x360Available = 2;
                }
                else{
                    x360Available = 1;
                }
            }
        });

        switchXONE.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    xoneAvailable = 2;
                }
                else{
                    xoneAvailable = 1;
                }
            }
        });

        switchNINTENDO.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    nintendoAvailable = 2;
                }
                else{
                    nintendoAvailable = 1;
                }
            }
        });

        btnAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(AddYourGameActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},REQUEST_CODE_GALLERY);
            }
        });



        btnAddYourGame.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                try{
                    String title = textTitle.getText().toString().trim();
                    String genre = textGenre.getText().toString().trim();
                    Integer year = Integer.parseInt(textYear.getText().toString());
                    String description = textDescription.getText().toString();
                    byte[] gameImage = imageViewToByte(image);

                    if(title.equals("")||genre.equals("")||year.equals("")||description.equals("")){
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.info_fields_empty), Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Boolean checkGame = openHelper.checkIfGameExists(title);
                        if (checkGame == true) {
                            if (year != null && year > 1900 && year < 2999) {
                                    if (gameImage.length < 256474) {
                                        openHelper.addGame(title, genre, year, description, pcAvailable, ps3Available, ps4Available, x360Available, xoneAvailable, nintendoAvailable, gameImage);
                                        System.out.println(gameImage.length);
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.action_game_add_success), Toast.LENGTH_SHORT).show();
                                        textTitle.setText("");
                                        textGenre.setText("");
                                        textYear.setText("");
                                        textDescription.setText("");
                                        image.setImageResource(R.mipmap.ic_launcher);
                                        switchPC.setChecked(false);
                                        switchPS3.setChecked(false);
                                        switchPS4.setChecked(false);
                                        switchX360.setChecked(false);
                                        switchXONE.setChecked(false);
                                        switchNINTENDO.setChecked(false);
                                    } else {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.info_file_too_large), Toast.LENGTH_SHORT).show();
                                    }

                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.info_invalid_release_year), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else{
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.action_game_add_failed), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                catch (Exception e){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.info_image_not_added), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        });

    }

    private byte[] imageViewToByte(ImageView image) {
            Bitmap bitmap = ((BitmapDrawable) image.getDrawable()).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 20, stream);
            byte[] byteArray = stream.toByteArray();
            return byteArray;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_CODE_GALLERY){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE_GALLERY);
            }
            else{
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.info_storage_no_permission), Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null){
            Uri uri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                image.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
