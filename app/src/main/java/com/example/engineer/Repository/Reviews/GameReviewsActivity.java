package com.example.engineer.Repository.Reviews;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.engineer.R;
import com.example.engineer.dataBase.Game.Game;
import com.example.engineer.dataBase.OpenHelper;
import com.example.engineer.dataBase.Review.Review;

import java.util.ArrayList;

public class GameReviewsActivity extends AppCompatActivity {


    private String loggedUser;
    private Game selectedGame;
    private String gameTitle;
    OpenHelper db;

    ArrayList<Review> listItem;
    ArrayAdapter adapter;
    ListView reviewsList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_reviews);

        db = new OpenHelper(this);
        loggedUser = getIntent().getExtras().getString("loggedUser");
        selectedGame = (Game)getIntent().getExtras().getSerializable("selectedGame");
        gameTitle = selectedGame.getTitle();
        reviewsList = findViewById(R.id.reviewList);

        listItem = new ArrayList<>();

        Cursor cursor = db.getReviewsByGame(gameTitle);
        if(cursor.getCount() == 0){
            Toast.makeText(getApplicationContext(), "No reviews for this game!", Toast.LENGTH_SHORT).show();
        }
        else {
            while (cursor.moveToNext()) {
                listItem.add(new Review(cursor.getInt(cursor.getColumnIndex("Id")), cursor.getString(cursor.getColumnIndex("AUTHOR")),cursor.getString(cursor.getColumnIndex("AUTHOR_NAME")), cursor.getString(cursor.getColumnIndex("GAME")), cursor.getString(cursor.getColumnIndex("reviewContent")),cursor.getInt(cursor.getColumnIndex("likesCount"))));
            }
        }

        ReviewListAdapter adapter = new ReviewListAdapter(this, R.layout.review_row, listItem);
        reviewsList.setAdapter(adapter);

    }

}
