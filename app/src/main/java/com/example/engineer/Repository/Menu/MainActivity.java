package com.example.engineer.Repository.Menu;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.engineer.R;
import com.example.engineer.Repository.AddYourGame.AddYourGameActivity;
import com.example.engineer.Repository.Encyclopedia.GamesActivity;
import com.example.engineer.Repository.Favorites.FavoritesActivity;
import com.example.engineer.Repository.Rankings.RankingActivity;
import com.example.engineer.dataBase.OpenHelper;
import com.firebase.client.Firebase;
//import com.firebase.client.Firebase;

public class MainActivity extends AppCompatActivity {
    private TextView txtInfo;
    private Firebase mRef;
    private String loggedUser;
    private OpenHelper openHelper;
    private SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        openHelper = new OpenHelper(this);
        loggedUser = getIntent().getExtras().getString("authorEmail");
        String loggedUserName = openHelper.getUserNameByEmail(loggedUser);
        String loggedUserNickame = openHelper.getNicknameByEmail(loggedUser);
        setContentView(R.layout.activity_main);
        TextView welcomeText = findViewById(R.id.textWelcome);
        welcomeText.setText(getString(R.string.info_welcome_text) + ' ' + loggedUserNickame + "!");

    }

    public void addYourGame(View view)
    {
        Intent intent = new Intent(MainActivity.this, AddYourGameActivity.class);
        intent.putExtra("loggedUser", loggedUser);
        startActivity(intent);
    }

    public void favorites(View view)
    {
        Intent intent = new Intent(MainActivity.this, FavoritesActivity.class);
        intent.putExtra("loggedUser", loggedUser);
        startActivity(intent);
    }

    public void encyclopedia(View view)
    {
        Intent intent = new Intent(MainActivity.this, GamesActivity.class);
        intent.putExtra("loggedUser", loggedUser);
        startActivity(intent);
    }

    public void rankings(View view)
    {
        Intent intent = new Intent(MainActivity.this, RankingActivity.class);
        intent.putExtra("loggedUser", loggedUser);
        startActivity(intent);
    }

    public void onLogoTap(View view){
        Toast myToast = Toast.makeText(getApplicationContext(), getString(R.string.toast_logo_tap), Toast.LENGTH_LONG);
        myToast.show();
    }
}
