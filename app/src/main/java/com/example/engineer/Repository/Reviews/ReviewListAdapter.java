package com.example.engineer.Repository.Reviews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.engineer.R;
import com.example.engineer.dataBase.OpenHelper;
import com.example.engineer.dataBase.Review.Review;

import java.util.ArrayList;
import java.util.List;

public class ReviewListAdapter extends ArrayAdapter<Review> {
    OpenHelper db;
    private Context mContext;
    int mResource;

    public ReviewListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Review> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Integer id = getItem(position).getId();
        String user = getItem(position).getAuthor();
        String name = getItem(position).getAuthorName();
        String content = getItem(position).getReviewContent();
        String game = getItem(position).getGame();
        Integer likesCount = getItem(position).getLikesCount();

        Review review = new Review(id, user, name, game, content, likesCount);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView textUser = (TextView) convertView.findViewById(R.id.textUser);
        TextView textContent = (TextView) convertView.findViewById(R.id.textContent);

        textUser.setText(name);
        textContent.setText(content);

        return convertView;

    }
}
