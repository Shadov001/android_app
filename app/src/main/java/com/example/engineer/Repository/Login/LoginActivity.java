package com.example.engineer.Repository.Login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.engineer.Repository.AESUtils.AESUtils;
import com.example.engineer.Repository.Menu.MainActivity;
import com.example.engineer.R;
import com.example.engineer.Repository.Register.RegisterActivity;
import com.example.engineer.dataBase.DataManager;
import com.example.engineer.dataBase.OpenHelper;

public class LoginActivity extends AppCompatActivity {

    public static final String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-z]{2,}";
    private EditText txtEmail;
    private EditText txtPassword;
    private Button btnLogin;
    private TextView txtValidate;
    private Boolean isValidEmail = false;

    DataManager dataManager;
    OpenHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        db = new OpenHelper(this);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        txtValidate = (TextView)findViewById(R.id.txtValidate);
        txtEmail.setText("");
        txtPassword.setText("");

        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String email = txtEmail.getText().toString();
                String decryptedPassword = txtPassword.getText().toString();
                String encryptedPassword;
                try{
                    encryptedPassword = AESUtils.encrypt(decryptedPassword);
                    Boolean checkLogin = db.loginCheck(email, encryptedPassword);
                    if(checkLogin){
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.action_info_logged), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("authorEmail", email);
                        startActivity(intent);
                        txtEmail.setText("");
                        txtPassword.setText("");
                    }
                    else{
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.action_info_wrong_login_data), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

    }

    public void register(View view)
    {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

}
