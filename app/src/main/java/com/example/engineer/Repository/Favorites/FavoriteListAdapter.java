package com.example.engineer.Repository.Favorites;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.engineer.R;
import com.example.engineer.dataBase.Favorite.Favorite;
import com.example.engineer.dataBase.Review.Review;

import java.util.ArrayList;
import java.util.List;

public class FavoriteListAdapter extends ArrayAdapter<Favorite> {

    private Context mContext;
    int mResource;

    public FavoriteListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Favorite> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Integer id = getItem(position).getId();
        String user = getItem(position).getUser();
        String genre = getItem(position).getGenre();
        String game = getItem(position).getTitle();
        Integer year = getItem(position).getYear();

        Favorite favorite = new Favorite(id, game, genre, year, user);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView textTitle = (TextView) convertView.findViewById(R.id.textTitle);
        TextView textGenre = (TextView) convertView.findViewById(R.id.textGenre);
        TextView textYear = (TextView) convertView.findViewById(R.id.textYear);

        textTitle.setText(game);
        textGenre.setText(genre);
        textYear.setText(Integer.toString(year));

        return convertView;

    }
}
