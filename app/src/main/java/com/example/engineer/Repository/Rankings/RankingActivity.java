package com.example.engineer.Repository.Rankings;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.engineer.R;
import com.example.engineer.Repository.Encyclopedia.DetailGameActivity;
import com.example.engineer.Repository.Encyclopedia.GamesActivity;
import com.example.engineer.Repository.Encyclopedia.GamesAdapter;
import com.example.engineer.dataBase.Game.Game;
import com.example.engineer.dataBase.OpenHelper;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;

public class RankingActivity extends AppCompatActivity {

    private RecyclerView gamesRecyclerView;
    private RecyclerView.Adapter gamesAdapter;
    ArrayList<Game> gamesData;
    private ArrayList<String> mGamesData = new ArrayList<>();
    private String loggedUser;
    private String gameTitle;
    private byte[] gameImage;
    private Boolean checkIfIsInFavorites;
    private Boolean checkIfLiked;
    OpenHelper db;
    DatabaseReference databaseGames;
    ListView listViewGames;
    List<Game> gamesList;

    GridView gridView;
    ArrayList<Game> list;
    GamesAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_games);

        loggedUser = getIntent().getExtras().getString("loggedUser");
        db = new OpenHelper(this);

        gamesRecyclerView = findViewById(R.id.gamesRecyclerView);
        gamesData = new ArrayList<>();
        gamesAdapter = new RankingAdapter(gamesData);
        ((RankingAdapter) gamesAdapter).setRankingActivity(this);


        Cursor cursor = db.getRanking();
        gamesData.clear();
        if(cursor.getCount() == 0){
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.info_no_games), Toast.LENGTH_SHORT).show();
        }
        else {
            while (cursor.moveToNext()) {
                gamesData.add(new Game(cursor.getInt(cursor.getColumnIndex("Id")),
                        cursor.getString(cursor.getColumnIndex("gameTitle")),
                        cursor.getString(cursor.getColumnIndex("gameGenre")),
                        cursor.getInt(cursor.getColumnIndex("year")),
                        cursor.getString(cursor.getColumnIndex("gameDescription")),
                        cursor.getInt(cursor.getColumnIndex("gamePC")),
                        cursor.getInt(cursor.getColumnIndex("gamePS3")),
                        cursor.getInt(cursor.getColumnIndex("gamePS4")),
                        cursor.getInt(cursor.getColumnIndex("gameXONE")),
                        cursor.getInt(cursor.getColumnIndex("gameX360")),
                        cursor.getInt(cursor.getColumnIndex("gameNINTENDO")),
                        cursor.getInt(cursor.getColumnIndex("likesCount")),
                        cursor.getBlob(cursor.getColumnIndex("image")),
                        cursor.getDouble(cursor.getColumnIndex("rating"))
                ));
                System.out.println(cursor.getFloat(cursor.getColumnIndex("rating")));
            }
            //adapter.notifyDataSetChanged();
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        gamesRecyclerView.setAdapter(gamesAdapter);
        gamesRecyclerView.setLayoutManager(layoutManager);

    }

}
