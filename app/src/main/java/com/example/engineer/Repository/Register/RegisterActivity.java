package com.example.engineer.Repository.Register;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.engineer.Repository.AESUtils.AESUtils;
import com.example.engineer.Repository.Login.LoginActivity;
import com.example.engineer.R;
import com.example.engineer.dataBase.DataManager;
import com.example.engineer.dataBase.OpenHelper;

public class RegisterActivity extends AppCompatActivity {

    private OpenHelper openHelper;
    private SQLiteDatabase db;
    private DataManager dataManager;
    private Boolean isValidEmail = false;
    private Boolean isNicknameInUse;
    public static final String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-z]{2,}";
    EditText editEmail, editName, editSurname, editPassword, editCheckPassword, editNickname;
    Button btnRegister;
    TextView textValidate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        openHelper = new OpenHelper(this);
        db = openHelper.getWritableDatabase();
        dataManager = new DataManager(db);
        editEmail = (EditText) findViewById(R.id.Email);
        editPassword = (EditText) findViewById(R.id.Password);
        editNickname = (EditText) findViewById(R.id.Nickname);
        editName = (EditText) findViewById(R.id.Name);
        editSurname = (EditText) findViewById(R.id.Surname);
        editCheckPassword = (EditText) findViewById(R.id.CheckPassword);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        textValidate = (TextView)findViewById(R.id.textValidate);
        textValidate.setText("");

        btnRegister.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String s1 = editEmail.getText().toString();
                String s2 = editPassword.getText().toString();
                String s3 = editName.getText().toString();
                String s4 = editSurname.getText().toString();
                String s5 = editCheckPassword.getText().toString();
                String s6 = editNickname.getText().toString();
                isValidEmail = (s1.matches(emailPattern) && s1.length() > 0);
                isNicknameInUse = openHelper.checkIfNicknameInUse(s6);
                if(s1.equals("")||s2.equals("")||s3.equals("")||s4.equals("")||s5.equals("")||s6.equals("")){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.info_fields_empty), Toast.LENGTH_SHORT).show();
                }
                else {
                    if(!isValidEmail) {
                        //textValidate.setTextColor(Color.rgb(200,0,0));
                        //textValidate.setText(R.string.info_email_validation_failed);
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.info_email_validation_failed),
                                Toast.LENGTH_LONG).show();
                    }
                    else {
                        if (!isNicknameInUse){
                            if (s2.length() < 5) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.invalid_password), Toast.LENGTH_SHORT).show();
                            } else {
                                if (s2.equals(s5)) {
                                    Boolean checkEmail = openHelper.checkEmail(s1);
                                    if (checkEmail == true) {
                                        try {
                                            String encryptedPassword = AESUtils.encrypt(s2);
                                            dataManager.addNewUser(editEmail.getText().toString(),
                                                    encryptedPassword,
                                                    s6,
                                                    0,
                                                    editName.getText().toString(),
                                                    editSurname.getText().toString(),
                                                    0.0);
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.action_register_success), Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                            startActivity(intent);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.action_register_failed), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.info_password_not_match), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                        else{
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.info_nickname_used), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

    }

}
