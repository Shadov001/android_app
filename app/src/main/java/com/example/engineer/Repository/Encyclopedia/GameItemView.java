package com.example.engineer.Repository.Encyclopedia;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.engineer.R;
import com.example.engineer.dataBase.OpenHelper;

public class GameItemView extends RecyclerView.ViewHolder {
    public TextView txtTitle;
    public TextView txtType;
    public TextView txtDescription;
    ImageView imageGame;
    private GamesActivity gamesActivity;

    public void setGamesActivity(GamesActivity gamesActivity) {
        this.gamesActivity = gamesActivity;

    }

    public GameItemView(View view){
        super(view);
        txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        txtType = (TextView) view.findViewById(R.id.txtType);
        imageGame = (ImageView) view.findViewById(R.id.imageGame);


        view.setOnClickListener(new View.OnClickListener(){
        @Override
        public void onClick(View w){
            GameItemView.this.gamesActivity.navigateToDetailPlace(getLayoutPosition());
            //navigate to "detail place"
        }

        });

    }
}
